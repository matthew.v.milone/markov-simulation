import turtle

from markovsim.MarkovChain import MarkovChain
from markovsim.Node import Node
from markovsim.Player import Player
from markovsim.constants import WEIGHT_INCREMENT


class GameController(object):
    def __init__(self, simulator, game_options):
        self.simulator = simulator
        self.time_per_iteration = game_options["Timer"]
        self.points_per_iteration = game_options["Points per Iteration"]
        self.model: MarkovChain = self.simulator.model
        self.players = [Player(1, self.model.tokens[0], "Alice"),
                        Player(2, self.model.tokens[1], "Bob")]
        self.pursuer: Player = self.players[0]
        self.evader: Player = self.players[1]

        self.active_player = self.players[0]
        self.round_score = 0
        self.keep_iterating = True

    def play_game(self):
        pass

    def play_round(self):
        print("hi") # TODO # DEBUG
        self.round_score = 0
        self.keep_iterating = True
        #while self.keep_iterating:
        #    print("keep on") # TODO # DEBUG
        #    self.simulator.window.ontimer(lambda: self.handle_iteration(), t=ITERATION_TIMER)
        self.pursuer, self.evader = self.evader, self.pursuer

    def is_caught(self):
        return self.pursuer.get_position() == self.evader.get_position()

    def change_edge_weights(self, node: Node):
        current_node: Node = self.active_player.get_position()
        if node in current_node.downstream_nodes:
            edge = self.model.find_edge(self.active_player.get_position(), node)
            weight_diff = WEIGHT_INCREMENT * self.active_player.spend_points(self.active_player.point_change_per_click)
            actual_change = self.model.change_edge_weights(edge, weight_diff)
            refund = (weight_diff - actual_change) / WEIGHT_INCREMENT
            self.active_player.gain_points(refund)
        else:
            self.simulator.message("You can only select nodes that you already have a chance of going to.")

    def handle_iteration(self):
        window = self.simulator.window
        self.round_score += 1
        for player in self.players:
            player.gain_points(self.points_per_iteration)
        self.simulator.update_point_boxes()
        self.simulator.iterate()
        self.model.reset_transitions()
        if self.is_caught():
            self.simulator.message(f"Score: {self.round_score}")
            self.keep_iterating = False
            self.round_score = 0
            #self.play_round()
        if self.keep_iterating:
            window.ontimer(self.handle_iteration, self.time_per_iteration)
