## A class defining an edge/connection in a Markov chain.
## Contains both model-layer instructions and view-layer instructions.


import math

from markovsim.helper import initialize_turtles
from markovsim.constants import BUFFER, TEXTBOX_HEIGHT


class Edge(object):
    def __init__(self, start_node, end_node, weight, scale):
        """
        A model object representing an edge in a Markov chain.
        :param start_node: the Node object at the tail end of the edge.
        :param end_node: the Node object at the lead of the edge.
        :param weight: the portion of start_node's share that will move along the edge each iteration.
        :param scale: the visual scale of the edge, in some respects.
        """
        self.startNode = start_node
        self.endNode = end_node
        self.weight = weight

        self._scale = scale
        self._turtles = initialize_turtles(["static", "edge_weight"])

        self.symmetric = False
        self.one_sided = True

    def draw(self):
        """
        Draw the arrow of the edge and determine the symmetry of the edge. Only needs to be called once.
        :return: None
        """
        bg_trt = self._turtles["static"]
        s_node = self.startNode
        e_node = self.endNode

        x_dis = e_node.x - s_node.x
        y_dis = e_node.y - s_node.y
        l2_dis = math.sqrt(x_dis**2 + y_dis**2)
        total_buffer = self._scale / 2 + BUFFER

        for edge in self.endNode.outgoing_edges:
            if edge.endNode == self.startNode:
                self.one_sided = False
                if edge.weight == self.weight:
                    self.symmetric = True
                break

        # Go to starting node.
        bg_trt.up()
        bg_trt.goto(s_node.x, s_node.y)

        # If the edge is non-reflexive...
        if s_node != e_node:
            bg_trt.seth(math.atan2(y_dis, x_dis))

            # Sidestep if edges are asymmetric and non-trivial.
            if self.weight and not self.one_sided and not self.symmetric:
                bg_trt.right(math.pi / 2)
                bg_trt.fd(self._scale / 6)
                bg_trt.left(math.pi / 2)
            bg_trt.fd(total_buffer)
            bg_trt.down()
            # Prevent duplicate drawing  # TODO
            # if not self.symmetric or s_node.x < e_node.x or s_node.x == e_node.x and s_node.y < e_node.y:
            #     bg_trt.down()
            edge_length = l2_dis - 2 * total_buffer
            bg_trt.fd(edge_length)
        # If the edge is reflexive...
        else:
            bg_trt.seth(math.atan2(s_node.y, s_node.x) - math.pi / 4)
            bg_trt.fd(total_buffer)
            bg_trt.down()
            bg_trt.circle(total_buffer, 3 * math.pi / 2)
        bg_trt.stamp()

        self.update_weight_label()

    def change_weight(self, new_weight):
        self.weight = new_weight

    def update_weight_label(self):
        """`
        Draw the edge weight.
        :return: None
        """
        weight_trt = self._turtles["edge_weight"]
        weight_trt.clear()
        s_node = self.startNode
        e_node = self.endNode
        x_dis = e_node.x - s_node.x
        y_dis = e_node.y - s_node.y
        l2_dis = math.sqrt(x_dis ** 2 + y_dis ** 2)
        total_buffer = self._scale / 2 + BUFFER
        edge_length = l2_dis - 2 * total_buffer

        weight_trt.goto(s_node.x, s_node.y)
        if s_node != e_node:
            # if self.weight and not self.one_sided and not self.symmetric:
            #     weight_trt.right(math.pi / 2)
            #     weight_trt.fd(self._scale / 6)
            #     weight_trt.left(math.pi / 2)
            weight_trt.seth(math.atan2(y_dis, x_dis))
            weight_trt.fd(total_buffer + .5 * edge_length)
            weight_trt.right(math.pi / 2)
            weight_trt.fd(1.5 * BUFFER)
            if self.weight and not self.one_sided and not self.symmetric:
                weight_trt.fd(self._scale / 6)
            weight_trt.seth(-math.pi / 2)
            weight_trt.fd(TEXTBOX_HEIGHT / 2)
        else:
            weight_trt.seth(math.atan2(s_node.y, s_node.x))
            weight_trt.fd(self._scale + BUFFER)
            weight_trt.seth(-math.pi / 2)
            weight_trt.fd(TEXTBOX_HEIGHT / 2)

        # Format the output string.
        num_str = str(round(self.weight, 4))
        if '.' in num_str:
            (whole_part, decimal_part) = num_str.split('.')
        elif num_str.startswith('.'):
            whole_part, decimal_part = '', num_str
        else:
            whole_part, decimal_part = num_str, ''
        if whole_part == '0':
            num_str = num_str[1:]

        # Print it.
        if not self.symmetric or s_node.x < e_node.x or s_node.x == e_node.x and s_node.y <= e_node.y:
            weight_trt.write(num_str, align="center")

    def clear_edge_weight(self):
        self._turtles["edge_weight"].clear()

    def update_ui(self, show_weights=True):
        if show_weights:
            self.update_weight_label()
        else:
            self.clear_edge_weight()
