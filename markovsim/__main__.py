import argparse
import sys

import yaml

from markovsim.Simulation import Simulation


CONFIG_DIRECTORY = "./configurations/"


def main():
    arg_parser = argparse.ArgumentParser(description="Simulate a Markov chain.")
    arg_parser.add_argument("config", metavar="configuration_file", type=str, help="The YAML file that stores information about the Markov chain and how it should be displayed.")
    arg_parser.add_argument("m", metavar="mode", default="CONTINUOUS", choices=["CONTINUOUS", "DISCRETE", "GAME"], nargs='?', help="The rules for updating the chain.")
    arg_parser.add_argument("-n", "--number_of_tokens", default=1, type=int, help="The number of objects being simulated by the chain at once.")

    args = arg_parser.parse_args()
    config_path = CONFIG_DIRECTORY + args.config
    mode = args.m.upper()
    if mode == "GAME":
        n_tokens = 2
    else:
        n_tokens = args.number_of_tokens

    with open(config_path, 'r') as config_file:
        config_data = yaml.safe_load(config_file)
    simulation = Simulation(config_data, mode, n_tokens)
    simulation.main_loop()

