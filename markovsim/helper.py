import math
import random
from turtle import Turtle


def initialize_turtles(turtle_names):
    """
    Provides a compact and uniform way to create turtle objects.
    :param turtle_names: a list of keys to reference the turtles.
    :return: a dictionary of turtle objects.
    """
    turtle_dict = {}
    for name in turtle_names:
        new_turtle = Turtle()
        new_turtle.ht()
        new_turtle.up()
        new_turtle.speed(0)
        new_turtle.radians()
        turtle_dict[name] = new_turtle
    return turtle_dict


def find_outcome(probabilities, debug=False):
    rand_num = random.uniform(0, 1)
    if debug:
        print(rand_num, probabilities)
    total = 0
    for i, p in zip(range(len(probabilities)), probabilities):
        total += p
        if total >= rand_num:
            return i


def calculate_points_on_circle(n, radius=1):
    """
    Finds a list of (x, y) coordinate pairs spaced evenly along a circle, beginning at the top (i.e. 90 degrees)
    and traveling clockwise.
    :param n: the number of points to be returned.
    :param radius: the radius of the circle.
    :return: a list of 2-tuples representing coordinate pairs.
    """
    points = []
    for i in range(n):
        angle = math.pi / 2 - i * 2 * math.pi / n
        x = math.cos(angle) * radius
        y = math.sin(angle) * radius
        points.append([x, y])
    return points


def align_turtles(moving_trt, reference_trt):
    # Copies the location and heading of the second turtle onto the first.
    moving_trt.up()
    moving_trt.goto(reference_trt.pos())
    moving_trt.seth(reference_trt.heading())
