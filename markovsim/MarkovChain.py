## A class containing a set of nodes and a set of edges, as well as a method for updating those objects.
## Contains both model-layer instructions and view-layer instructions.


from markovsim.Node import Node
from markovsim.Edge import Edge
from markovsim.Token import Token
from markovsim.helper import find_outcome
from markovsim.constants import ROUNDING_TOLERANCE, ITERATION_LIMIT, PURSUER_COLOR


class MarkovChain(object):
    def __init__(self, config, weight_method="CONTINUOUS", n_tokens=1):
        """
        A model object containing data that represents a Markov chain, as well as the means to update and display it.
        :param config: a dictionary object based on a config file that has been normalized by Simulation.process_config.
        :param weight_method: a code specifying the method used to update the node weights.
        :param n_tokens: the number of objects being simulated by the chain--e.g. the number of players in Monopoly.
        """
        self.options = config['Options']
        self._game_settings = config['Game Settings']
        self._original_transitions = config['Transitions']
        self.transitions = [row[:] for row in self._original_transitions]
        self.n_states = len(self.transitions)
        self.n_tokens = n_tokens
        # Set dynamic method.
        self._mode = weight_method
        self.distribution = config['Distribution']
        if self._mode == "DISCRETE":  # TODO: possibly obsolete.
            working_distribution = [0 for i in range(self.n_states)]
            for i in range(n_tokens):
                destination_node_index = find_outcome(self.distribution)
                working_distribution[destination_node_index] += 1
            self.distribution = working_distribution
        # Initialize toggle settings.
        self._show_edge_weights = True
        if self._mode == "GAME":
            self._show_node_weights = False
        else:
            self._show_node_weights = True
        self._show_node_fill = True

        # Create the model.
        self.iteration = 0
        self.nodes = []
        self.edges = []
        self.tokens = []
        self.create_model(config)

    def create_model(self, config):
        """
        Create the Markov chain's Node and Edge objects based on the configuration file,
        and then draw the model in its initial state.
        :param config: a normalized configuration object
        :return: None
        """
        # Create Node objects.
        names = config['Names']
        if len(names) == 0:
            names = ['' for i in range(self.n_states)]
        locations = config['Layout Details']
        scale = self.options['Dot Size']
        for i in range(self.n_states):
            name = names[i]
            x = locations[i][0]
            y = locations[i][1]
            new_node = Node(name, x, y, scale, self, i, self._mode)
            self.nodes.append(new_node)

        # Create tokens.
        if self._mode == "GAME":
            starting_locations = [self._game_settings["Red Start"], self._game_settings["Blue Start"]]
            for location in starting_locations:
                new_token = Token(location, self.distribution, self)
                self.tokens.append(new_token)
                node = new_token.get_position()
                node.add_token(new_token)
        else:
            for i in range(self.n_tokens):
                normalized_distribution = [i / self.n_tokens for i in self.distribution]
                node_index = find_outcome(normalized_distribution)
                new_token = Token(node_index, normalized_distribution, self)
                self.tokens.append(new_token)
                node = new_token.get_position()
                node.add_token(new_token)

        # Create Edge objects.
        for i in range(self.n_states):
            start_node = self.nodes[i]
            transition_weights = self.transitions[i]
            for j in range(self.n_states):
                weight = transition_weights[j]
                if weight == 0:
                    continue
                end_node = self.nodes[j]
                new_edge = Edge(start_node, end_node, weight, scale)
                start_node.outgoing_edges.append(new_edge)
                end_node.incoming_edges.append(new_edge)
                self.edges.append(new_edge)

        # Fill in Node attributes.
        for node in self.nodes:
            node.downstream_nodes = [edge.endNode for edge in node.outgoing_edges]
            node.upstream_nodes = [edge.startNode for edge in node.incoming_edges]

        # Draw the Markov chain in its initial state.
        for node in self.nodes:
            node.draw()
        for edge in self.edges:
            edge.draw()
        self.update_ui()

    def iterate(self, update_ui=True):
        """
        Advance the Markov chain by one iteration, and update the display of the nodes.
        :return: None
        """
        self.iteration += 1
        new_token_destinations = [token.calc_new_position() for token in self.tokens]
        new_token_distributions = [token.calc_new_distribution() for token in self.tokens]
        for i in range(self.n_tokens - 1, -1, -1):
            token = self.tokens[i]
            token.set_position(new_token_destinations.pop())
            token.set_distribution(new_token_distributions.pop())
        self.distribution = [node.get_weight() for node in self.nodes]
        if update_ui:
            self.update_ui()

    def iterate_until_stable(self):
        """
        Advance the Markov chain until it stabilizes to within a certain threshold, or times out.
        :return: the iteration number of the model at the time this method completes.
        """
        last_distribution = [0 for i in range(self.n_states)]
        while (max([abs(self.distribution[i] - last_distribution[i])
                    for i in range(self.n_states)]) > ROUNDING_TOLERANCE / 10
               or self._mode != "CONTINUOUS") and self.iteration < ITERATION_LIMIT:
            last_distribution = self.distribution[:]
            self.iterate(update_ui=False)
        self.update_ui()
        return self.iteration

    def toggle_weights(self, toggle_nodes=True, toggle_edges=True):
        if toggle_nodes:
            self._show_node_weights = not self._show_node_weights
        if toggle_edges:
            self._show_edge_weights = not self._show_edge_weights
        self.update_ui(update_nodes=toggle_nodes, update_edges=toggle_edges)

    def update_ui(self, update_nodes=True, update_edges=True):
        """
        Update the displays of the nodes and edges.
        :param update_nodes: controls whether the nodes are updated.
        :param update_edges: controls whether the edges are updated.
        :return: None
        """
        if update_nodes:
            for node in self.nodes:
                node.update_ui(self._show_node_weights, mode=self._mode)
            if self._mode == "GAME":
                self.tokens[1].get_position().update_ui(self._show_node_weights, mode=self._mode)
                self.tokens[0].get_position().update_ui(self._show_node_weights, mode=self._mode, color=PURSUER_COLOR)
        if update_edges:
            for edge in self.edges:
                edge.update_ui(show_weights=self._show_edge_weights)

    def change_edge_weights(self, edge: Edge, delta):
        """
        Changes the
        :param edge: the edge that is the target of the weight change.
        :param delta: the proposed change in the edge's weight.
        :return: None
        """
        # TODO: play with the bounded requirement. What do negative transition probabilities do?
        diff = min(max(edge.weight + delta, 0), 1) - edge.weight
        s = sum([other_edge.weight for other_edge in edge.startNode.outgoing_edges if other_edge is not edge])
        start_index = edge.startNode.index
        for other_edge in edge.startNode.outgoing_edges:
            if other_edge is not edge:
                other_edge.change_weight(other_edge.weight - diff * other_edge.weight / s)
            else:
                other_edge.change_weight(other_edge.weight + diff)
            end_index = other_edge.endNode.index
            self.transitions[start_index][end_index] = other_edge.weight
        edge.clear_edge_weight()
        for edge in self.edges:
            edge.update_ui(self._show_edge_weights)
        return diff

    def find_edge(self, start_node, end_node):
        for edge in start_node.outgoing_edges:
            if edge in end_node.incoming_edges:
                return edge
        else:
            return None

    def reset_transitions(self):
        self.transitions = [row[:] for row in self._original_transitions]
        for edge in self.edges:
            s_index = edge.startNode.index
            e_index = edge.endNode.index
            new_weight = self.transitions[s_index][e_index]
            edge.change_weight(new_weight)
        self.edges[0].clear_edge_weight()
        for edge in self.edges:
            edge.update_weight_label()

    def get_transitions(self, n):
        """
        Get the transition probabilities for a row number or a Node.
        :param n: a row number or a Node.
        :return: a list describing the probability of transitioning to each state.
        """
        if type(n) == int:
            row = n
        elif type(n) == Node:
            row = n.index
        else:
            raise ValueError("Error: n must be an integer or a Node.")
        return self.transitions[row]

    def get_node(self, n: int) -> Node:
        """
        Gets a node from a given node index.
        :param n: the node index.
        :return: the node corresponding to the index.
        """
        try:
            self.nodes[n]
        except IndexError:
            print(n)

        return self.nodes[n]
