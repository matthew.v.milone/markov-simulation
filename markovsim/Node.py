## A class defining a node/vertex in a Markov chain.
## Contains both model-layer instructions and view-layer instructions.


import math

from markovsim.helper import initialize_turtles
from markovsim.constants import BUFFER, TEXTBOX_HEIGHT, DEF_NODE_WEIGHT_COLOR


class Node(object):
    def __init__(self, name, x, y, scale, parent_chain, index, mode):
        """
        A model object representing a node/vertex in a Markov chain.
        :param name: the name of the node.
        :param x: the x-coordinate of the node.
        :param y: the y-coordinate of the node.
        :param scale: the visual scale of the node.
        :param parent_chain: the Markov chain that the Node belongs to.
        :param index: the a unique sequential number that makes it easier to specify this node.
        :param mode: the mode of the simulation.
        """

        self.name = name
        self.index = index
        self.chain = parent_chain
        self._chain_weight = self.chain.n_tokens
        self.outgoing_edges = []
        self.incoming_edges = []
        self.downstream_nodes = []
        self.upstream_nodes = []
        self._tokens = []

        self._mode = mode

        self.x = x
        self.y = y
        self.scale = scale

        self._turtles = initialize_turtles(["static", "node_weight", "node_fill"])

    def get_weight(self):
        if self._mode == "CONTINUOUS":
            return sum((token.get_distribution()[self.index] for token in self.chain.tokens))
        else:
            return len(self._tokens)

    def remove_token(self, token):
        self._tokens.remove(token)

    def add_token(self, token):
        self._tokens.append(token)

    def draw(self):
        """
        Draw the static, background circle of the node and write the node's name. Only needs to be called once.
        :return: None
        """
        bg_trt = self._turtles["static"]
        # Draw reference dot
        bg_trt.penup()
        bg_trt.goto(self.x, self.y)
        bg_trt.dot(self.scale, "#606060")
        bg_trt.dot(self.scale - BUFFER / 4, "#E0E0E0")
        # Draw node name
        bg_trt.goto(self.x, self.y + self.scale / 2 + BUFFER / 2)
        bg_trt.write(self.name, align="center", font=("Arial", 8, "bold"))

    def update_ui(self, show_weight=True, mode="CONTINUOUS", color=DEF_NODE_WEIGHT_COLOR):
        self.update_fill(color=color)
        if show_weight:
            self.update_weight_label(mode=mode)
        else:
            self._turtles["node_weight"].clear()

    def update_fill(self, color=DEF_NODE_WEIGHT_COLOR):
        """
        Draw the node's fill dot.
        :return: None
        """
        fill_trt = self._turtles["node_fill"]
        fill_trt.clear()
        fill_trt.penup()
        fill_trt.goto(self.x, self.y)
        fill_trt.dot(math.sqrt(self.get_weight() / self._chain_weight) * self.scale, color)

    def update_weight_label(self, mode="CONTINUOUS"):
        """
        Draw the node's weight/share.
        :return: None
        """
        weight_trt = self._turtles["node_weight"]
        weight_trt.clear()
        ## Other node weight locations
        # self.iter_turtle.goto(self.x + self.scale / 2 + BUFFER / 2, self.y - TEXTBOX_HEIGHT / 2) # Right
        # self.iter_turtle.goto(self.x + 3*self.scale/8, self.y - 3*self.scale/8 - 10) # Bottom-right
        # self.iter_turtle.goto(self.x, self.y - self.scale/2 - BUFFER/2 - TEXTBOX_HEIGHT) # Bottom
        weight_trt.goto(self.x, self.y - TEXTBOX_HEIGHT / 2)  # Center
        if mode == "CONTINUOUS":
            num_str = str(round(self.get_weight(), 4))
            if len(num_str) > 1 and num_str[0] == "0":
                num_str = num_str[1:]
        else:
            num_str = str(int(self.get_weight()))
        weight_trt.write(num_str, align="center", font=("Arial", 8, "bold"))
