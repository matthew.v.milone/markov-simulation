## A class containing the attributes and actions that can be taken by each player in Marko Polo.

from markovsim.Token import Token


class Player(object):
    def __init__(self, player_id, token, name=""):
        self.id = player_id
        if name:
            self.name = name
        else:
            self.name = str(player_id)
        self.token: Token = token
        self.points = 10
        self.point_change_per_click = 10

    def get_position(self):
        return self.token.get_position()

    def gain_points(self, points) -> None:
        self.points += points

    def spend_points(self, points) -> int:
        points_spent = min(points, self.points)
        self.points -= points_spent
        return points_spent
