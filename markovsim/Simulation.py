## The class defines an object that performs a single Markov simulation, including the high-level Turtle calls.
## Among other tasks, this class is responsible for handling user input.
## It acts as both a view object and a controller object.


import math
import turtle

from markovsim.MarkovChain import MarkovChain
from markovsim.GameController import GameController
from markovsim.helper import initialize_turtles
from markovsim.constants import *


class Simulation(object):
    def __init__(self, config, mode="CONTINUOUS", n_tokens=1):
        """
        Creates the window, Turtles, and model necessary to the simulation, and provides the methods to update them.
        :param config: a dictionary object derived using yaml.safe_load on a config file.
        """
        turtle.delay(0)  # TODO
        self.mode = mode
        self.n_tokens = n_tokens

        self._turtles = initialize_turtles(["iteration_box", "point_boxes", "static"])

        self.iteration = 0
        self.n = len(config["Transitions"])
        self.normalized_config = self.process_config(config)

        self.window = turtle.Screen()
        self.window.tracer(0)
        self.model = MarkovChain(self.normalized_config, weight_method=self.mode, n_tokens=n_tokens)
        if self.mode == "GAME":
            self.game_controller = GameController(self, self.normalized_config["Game Settings"])

        # Establish window and screen dimensions.
        display_width = self.window.window_width()
        display_height = self.window.window_height()
        farthest_x = max(abs(node.x) for node in self.model.nodes)
        farthest_y = max(abs(node.y) for node in self.model.nodes)
        if farthest_x > display_width / 2 or farthest_y > display_height / 2:
            screen_buffer = 4 * self.normalized_config["Options"]["Dot Size"]
            self.window.screensize(2 * farthest_x + screen_buffer, 2 * farthest_y + screen_buffer)
        self.window.setup(width=1.0, height=1.0)  # Quasi-maximize window on start-up

        self.update_iteration_box()
        if mode == "GAME":
            self.update_point_boxes()

    def process_config(self, config):
        """
        Returns a completed, normalized, and validated version of a given config file.
        :param config: an unfiltered object produced from the YAML configuration file.
        :return: a dictionary that is structured like config.
        """
        normalized_config = {}

        if "Options" in config:
            options = config["Options"]
            normalized_config["Options"] = config["Options"]
        else:
            options = {}
            normalized_config["Options"] = {}
        norm_options = normalized_config["Options"]

        # Establish the dot size.
        if "Dot Size" in options:
            normalized_config["Options"]["Dot Size"] = options["Dot Size"]
        else:
            normalized_config["Options"]["Dot Size"] = math.sqrt(TOTAL_DOT_SIZE / (self.n * math.pi))

        # Establish the layout size.
        if "Layout Radius" in options:
            normalized_config["Options"]["Layout Radius"] = options["Layout Radius"]
        else:
            normalized_config["Options"]["Layout Radius"] = LAYOUT_RADIUS

        # Normalize the node names.
        if "Names" not in config:
            normalized_config["Names"] = []
        else:
            normalized_config["Names"] = config["Names"]
        norm_names = normalized_config["Names"]
        num_names = len(norm_names)
        if num_names != 0 and num_names != self.n:
            raise ValueError(f"Error: names list should either be absent or complete. "
                             f"There are {self.n} nodes, but {num_names} names.")

        # Standardize the Transitions item into a matrix format.
        transitions = config["Transitions"]
        if len(transitions) == 0:
            raise ValueError("Transitions item requires at least one row.")
        is_list = False
        for row in transitions:
            if len(row) > 0 and isinstance(row[0], list):
                is_list = True
                break
        if is_list:
            key_type = "int"
            if type(transitions[0][0][0]) is str:
                key_type = "str"
            n = len(transitions)
            transition_matrix = [[0 for i in range(n)] for j in range(n)]
            for row, i in zip(transitions, range(n)):
                for destination, value in row:
                    if key_type == "str":
                        destination = normalized_config["Names"].index(destination)
                    transition_matrix[i][destination] = value
        else:
            transition_matrix = transitions
        # Input validation for transition matrix
        for row in transition_matrix:
            s = sum(row)
            if s < 1 - ROUNDING_TOLERANCE or s > 1 + ROUNDING_TOLERANCE:
                raise ValueError(f"Error: sum of transition probabilities must be 1 for each row. "
                                 f"Actual sum of row {row}: {s}.")
        normalized_config["Transitions"] = transition_matrix

        # Normalize the initial distribution.
        if "Distribution" not in options:
            distribution = "Single"
            config["Distribution Details"] = 0
        else:
            distribution = options["Distribution"]
        if distribution == "Custom":
            s = sum(config["Distribution Details"])
            if s < 1 - ROUNDING_TOLERANCE or s > 1 + ROUNDING_TOLERANCE:
                raise ValueError("Error: initial distribution must sum to 1.")
            normalized_config["Distribution"] = config["Distribution Details"]
        elif distribution == "Uniform":
            inverse_n = 1 / self.n
            normalized_config["Distribution"] = [inverse_n for i in range(self.n)]
        elif distribution == "Single":
            details = config["Distribution Details"]
            working_distribution = [0 for i in range(self.n)]
            initial_index = None
            if type(details) is int:
                initial_index = details
            elif type(details) is str:
                initial_index = normalized_config["Names"].index(details)
            working_distribution[initial_index] = 1
            normalized_config["Distribution"] = working_distribution
        else:
            raise ValueError(f"Error: unrecognized distribution \"{distribution}\".")

        # Calculate the locations of nodes.
        static_trt = self._turtles["static"]
        if "Layout" not in options:
            layout = "Circle"
        else:
            layout = options["Layout"]
        if layout == "Custom":
            normalized_locations = config["Layout Details"]
        elif layout == "Circle":
            static_trt.goto(0, norm_options["Layout Radius"])
            static_trt.seth(0)
            arc_angle = 2 * math.pi / self.n
            normalized_locations = []
            for i in range(self.n):
                x, y = static_trt.xcor(), static_trt.ycor()
                normalized_locations.append([x, y])
                static_trt.circle(-1 * norm_options["Layout Radius"], extent=arc_angle)
        elif layout == "Polygon":
            n_sides = config["Layout Details"]
            static_trt.goto(0, 0)
            static_trt.seth(math.pi / 2 + math.pi / n_sides)
            static_trt.fd(norm_options["Layout Radius"])
            static_trt.seth(0)
            normalized_locations = []
            nodes_per_side = [0 for i in range(n_sides)]
            for i in range(self.n):
                nodes_per_side[i % n_sides] += 1
            for nodes_on_this_side in nodes_per_side:
                for i in range(nodes_on_this_side):
                    x, y = static_trt.xcor(), static_trt.ycor()
                    normalized_locations.append([x, y])
                    static_trt.fd(norm_options["Layout Radius"] * 2 * math.sin(math.pi / n_sides) / nodes_on_this_side)
                static_trt.right(2 * math.pi / n_sides)
        elif layout == "Rounded":
            n_sides = config["Layout Details"]
            pivot_points = []
            static_trt.goto(0, -1 * norm_options["Layout Radius"])
            static_trt.seth(math.pi)
            arc_angle = 2 * math.pi / n_sides
            for i in range(n_sides):
                x, y = static_trt.xcor(), static_trt.ycor()
                pivot_points.append((x, y))
                static_trt.circle(-1 * norm_options["Layout Radius"], extent=arc_angle)
            normalized_locations = []
            nodes_per_side = [0 for i in range(n_sides)]
            for i in range(self.n):
                nodes_per_side[i % n_sides] += 1
            for i in range(n_sides):
                pivot_x, pivot_y = pivot_points[i][0], pivot_points[i][1]
                corner_x = norm_options["Layout Radius"] * math.cos(arc_angle/2 + math.pi/2 - i*arc_angle)
                corner_y = norm_options["Layout Radius"] * math.sin(arc_angle/2 + math.pi/2 - i*arc_angle)
                static_trt.seth(math.atan2(corner_y - pivot_y, corner_x - pivot_x) - math.pi / 2)
                static_trt.goto(corner_x, corner_y)
                nodes_on_this_side = nodes_per_side[i]
                minor_arc_radius = math.sqrt((corner_x - pivot_x) ** 2 + (corner_y - pivot_y) ** 2)
                minor_arc_angle = arc_angle / (2 * nodes_on_this_side)  # Inclusion of the 2 in the denominator is due to the central angle theorem.
                for j in range(nodes_on_this_side):
                    x, y = static_trt.xcor(), static_trt.ycor()
                    normalized_locations.append([x, y])
                    static_trt.circle(-1 * minor_arc_radius, extent=minor_arc_angle)
        # elif layout == "Layered":
        #     # if self.n < 4:
        #     #     nodes_per_layer = [max(self.n, 0)]
        #     # elif self.n < 12:
        #     #     nodes_per_layer = [max(self.n - 8, 0), min(self.n, 8)]
        #     # else:
        #     #     nodes_per_layer = [max(self.n - 8, 0), max(self.n - 11, 0), min(self.n, 16)]
        #     nodes_per_layer = []
        #     next_layer = 3
        #     unassigned_nodes = self.n
        #     while unassigned_nodes > 0:
        #         nodes_in_this_layer = min(unassigned_nodes, next_layer)
        #         nodes_per_layer.append(nodes_in_this_layer)
        #         unassigned_nodes -= nodes_in_this_layer
        #         next_layer *= 2
        #     normalized_locations = []
        #     for layer, i in zip(nodes_per_layer, range(len(nodes_per_layer))):
        #         print(layer, nodes_per_layer)
        #         layer_coords = calculate_points_on_circle(layer, (i + 1) * LAYOUT_RADIUS / 2)
        #         normalized_locations.extend(layer_coords)
        #     normalized_config["Locations"] = normalized_locations
        else:
            raise ValueError(f"Error: unrecognized layout \"{layout}\".")
        normalized_config["Layout Details"] = normalized_locations

        ### Game Settings ###
        if "Game Settings" in config:
            game_settings = config["Game Settings"]
            norm_game_settings = config["Game Settings"]
        else:
            game_settings = {}
            norm_game_settings = {}
        normalized_config["Game Settings"] = norm_game_settings

        # Pursuer Start Location
        if "Red Start" not in game_settings:
            red_start = 0
        else:
            red_start = game_settings["Red Start"]
            if type(red_start) is str:
                red_start = normalized_config["Names"].index(red_start)
        norm_game_settings["Red Start"] = red_start

        # Evader Start Location
        if "Blue Start" not in game_settings:
            blue_start = -1
        else:
            blue_start = game_settings["Blue Start"]
            if type(blue_start) is str:
                blue_start = normalized_config["Names"].index(blue_start)
        norm_game_settings["Blue Start"] = blue_start

        # Iteration Timer
        if "Timer" not in game_settings:
            timer = ITERATION_TIMER
        else:
            timer = game_settings["Timer"]
        norm_game_settings["Timer"] = timer

        # Iteration Points
        if "Points per Iteration" not in game_settings:
            points_per_iteration = POINT_GAIN_PER_ITERATION
        else:
            points_per_iteration = game_settings["Points per Iteration"]
        norm_game_settings["Points per Iteration"] = points_per_iteration

        ### Return ###
        return normalized_config

    def main_loop(self):
        """
        Defines the simulation's key bindings and initiates the listening loop.
        :return: None
        """
        if self.mode != "GAME":
            turtle.onkey(self.iterate, "space")
            turtle.onkey(self.iterate_until_stable, "Return")
        else:
            iteration_timer = self.normalized_config["Game Settings"]["Timer"]
            self.window.ontimer(self.game_controller.handle_iteration, iteration_timer)

        turtle.onkey(self.toggle_weights, "w")
        turtle.onscreenclick(self.handle_click)

        turtle.listen()
        turtle.mainloop()

    def handle_click(self, click_x, click_y):
        click_coords = (click_x, click_y)
        clicked_node = None
        for node in self.model.nodes:
            node_coords = (node.x, node.y)
            if math.dist(click_coords, node_coords) <= node.scale / 2:
                clicked_node = node
                break
        if clicked_node and self.mode == "GAME":
            self.game_controller.change_edge_weights(clicked_node)
            self.update_point_boxes()

    def iterate(self):
        """
        Advances the simulation by exactly one iteration.
        :return: None
        """
        self.iteration += 1
        self.update_iteration_box()
        self.model.iterate()
        #if self.mode == "GAME":
        #    self.game_controller.handle_iteration()

    def iterate_until_stable(self):
        """
        Advances the simulation until the Markov chain's distribution stabilizes to within a certain threshold.
        :return: None
        """
        iterations = self.model.iterate_until_stable()
        self.iteration = iterations
        self.update_iteration_box()

    def update_iteration_box(self):
        """
        Updates the visual iteration counter in the top-left corner of the screen.
        :return: None
        """
        iter_box_turtle = self._turtles["iteration_box"]
        iter_box_turtle.clear()
        canvas = self.window.getcanvas()
        # window_x0 is the x-coordinate of the bottom-left corner of the window, relative to the center of the canvas.
        window_x0 = canvas.canvasx(0)
        window_y0 = canvas.canvasy(0)
        new_x = window_x0 + 4 + 2 * BUFFER
        new_y = - window_y0 - TEXTBOX_HEIGHT - 2 * BUFFER
        iter_box_turtle.goto(new_x, new_y)
        iter_box_turtle.write(f"Iterations: {self.iteration}")

    def update_point_boxes(self):
        points_turtle = self._turtles["point_boxes"]
        points_turtle.clear()
        canvas = self.window.getcanvas()
        window_x0 = canvas.canvasx(0)
        window_y0 = canvas.canvasy(0)
        new_x = window_x0 + 4 + 2 * BUFFER
        new_y = window_y0 + 2 * BUFFER
        points_turtle.goto(new_x, new_y)
        red_points = self.game_controller.pursuer.points
        points_turtle.write(f"Points: {int(red_points)}")

    def message(self, text):
        print(text)

    def toggle_weights(self, toggle_nodes=True, toggle_edges=True):
        if self.mode == "GAME":
            toggle_nodes = False
        self.model.toggle_weights(toggle_nodes, toggle_edges)
