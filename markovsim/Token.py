## Models an object that is subject to a Markov process.

from markovsim.Node import Node
from markovsim.helper import find_outcome


class Token(object):
    def __init__(self, starting_location, initial_distribution, markov_chain):
        self.chain = markov_chain
        self._n_nodes = markov_chain.n_states
        self._distribution = initial_distribution
        self._location: Node = markov_chain.get_node(starting_location)
        self.history = [self._location]

    def get_index(self) -> int:
        return self.get_position().index

    def get_position(self) -> Node:
        return self._location

    def get_distribution(self):
        return self._distribution

    def set_position(self, destination: Node) -> None:
        self._location.remove_token(self)
        self._location = destination
        self.history.append(destination)
        destination.add_token(self)

    def set_distribution(self, new_distribution) -> None:
        self._distribution = new_distribution

    def calc_new_position(self):
        s_node_index = self.get_position().index
        transitions = self.chain.get_transitions(s_node_index)
        e_node_index = find_outcome(transitions)
        e_node = self.chain.get_node(e_node_index)
        return e_node

    def calc_new_distribution(self):
        working_weights = [0 for i in range(self._n_nodes)]
        for i in range(self._n_nodes):
            e_node = self.chain.get_node(i)
            distribution = self.get_distribution()
            working_weights[i] = sum((edge.weight * distribution[edge.startNode.index] for edge in e_node.incoming_edges))
        return working_weights
