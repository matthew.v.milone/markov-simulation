### Overview
###### Summary
This package simulates a [Markov system](https://en.wikipedia.org/wiki/Markov_chain), taking an initial state and transition matrix as input. Using Python's Turtle module, it displays the initial state, and then allows the user to make the system progress via the Space and Enter keys.

###### Usage
If you do not have Python installed on your computer, then use the command prompt to navigate to the directory containing MarkovSim_v1-A-B.exe (where *A* and *B* are the major and minor version numbers, respectively) and run the following command:

<code>MarkovSim_v1-A-B.exe <config_file.yaml></code>

...where <code><config_file.yaml></code> is a stand-in for the name of your configuration file. If you do have Python installed on your computer, then you may use the following command, instead:

<code>python MarkovSim.py <config_file.yaml></code>

The configuration file name is the only required argument. Configuration files must be located in a folder called "configurations"--which should, in turn, be located in the same folder as the .py or .exe file.

To control the number of tokens being simulated at once, add the flag <code>-n N</code>, where <code>N</code> is the number of tokens. 

To run the program as a Monte Carlo simulation, add the word <code>DISCRETE</code> as an additional argument. For the game mode, Marko Polo, add the word <code>GAME</code>, instead.

For further help running the script, use one of the following commands:

<code>python MarkovSim.py -h</code>

<code>MarkovSim_v1-A-B.exe</code>

### Using the Simulation Window
When the program is run with a valid configuration file, a window will appear with a graphical depiction of the Markov chain. Node and edge weights only display up to four digits of precision, but the back-end, behind-the-scenes precision is high. 

To make the system progress by one iteration, press the Space key. To make the system progress until it stabilizes, or progresses until the 1000th iteration if the system is not stable, press the Enter key.

To toggle the visibility of the node and edge weights, press "w".

### Configuration Files
A configuration file contains the mathematical information about a Markov chain, as well as optional instructions for displaying it.

For new users, I recommend viewing and using the following configuration files before any others: "minimalist_config.yaml", "sample_square.yaml", and "simple_weather.yaml".   

Once you are familiar with the features of this software and the specifications of the configuration files, I encourage you to create your own Markov chains. To do this, create a document using a simple text editor. When you save it, save it as a .yaml file. Be sure to save it in a directory called "configurations" where the sample files are stored.

Each configuration file requires a Transitions list/matrix. The file may require additional items, depending on the options used. 

###### Transitions
_A 2D array containing the Markov chain's transition probabilities. In a sense, this is the core of the Markov system. It can take one of two formats._
- _Dense_: a complete matrix. Each node gets a full row and column, and each transition is one cell in the matrix.
- _Sparse_: each node gets its own row, but only the transition probabilities which are greater than 0 must be listed. Each transition is a tuple (implemented as a list) of the format \[\<destination node\>, \<probability\>\]. The destination node can be specified using either its name or index.

Which matrix format is used is not explicitly specified by the user; it is automatically determined from the syntax of the matrix.

###### Options
_A set of parameters that controls the visual style and layout of the Markov chain. The use of this item is, itself, optional, but it is recommended in many cases._
- _Dot Size:_ the visual scale of the nodes. Default is based on the number of nodes.
- _Layout Radius:_ the visual scale of the chain for non-custom layouts. Default is 200.
- _Distribution_: determines the starting share of each node. Default is Single, with the starting node defaulting to the first node.
    - _Custom_: uses a list of floats specified in Distribution Details.
    - _Uniform_: splits the initial share evenly among each node.
    - _Single_: one node has an initial share of 1, and the rest have 0. Node is specified in Distribution Details using its name or index.
- _Layout_: the visual arrangement of the nodes. Default is Circle.
    - _Custom_: uses a list of coordinate pairs specified in Layout Details. 
    - _Circle_: arranges the nodes in the shape of a regular, n-sided polygon with the first node at the top, progressing clockwise.
    - _Polygon_: creates a regular polygon with a flat edge along the top, progressing clockwise. Unlike Circle, this option allows for more than two nodes along the same side. Layout Details is the number of sides, and it should be an integer of 3 or more.
    - _Rounded_: similar to the Polygon layout, but each side of the polygon bulges outward. Odd values of Layout Details produce [Reuleaux polygons](https://en.wikipedia.org/wiki/Reuleaux_polygon).

###### Game Settings
_A set of parameters that controls the balance and pace of Marko Polo. This is an optional item, since each setting has a default, and it has no effect outside of the game mode._
- _Red Start_: the node that the pursuing player begins the game on. Can be specified by an index or a name.
- _Blue Start_: the node that the target token begins on.
- _Timer_: time in milliseconds between iteration.
- _Points per Iteration_: the number of points gained per iteration.

###### Other Items
_Additional configuration file entries that may be necessary based on the options chosen._ 
- _Names:_ a list of names of the nodes. If this item is included, then it must be complete or empty. 
- _Distribution Details:_ a list, number, or string containing additional information about the initial distribution. See the section on Distribution options for details.
- _Layout Details_: a number or list containing additional information about the layout. See the section on Layout options for details.

###### Format and Technical Details
These items may be listed within the configuration file in any order, and with any number of comments or blank lines in-between. See [here](https://camel.readthedocs.io/en/latest/yamlref.html) for more information about YAML syntax. 

### Modes
This program has three modes. The mode is selected when the program is run with the command line (see above). The default mode is <code>CONTINUOUS</code>.

###### Continuous
Intuitively, it can be thought of as a quantity of liquid being pumped along channels (edges) between different pools (states/nodes/vertices).

When updating, the new weight of each node is equal to the [expected value](https://en.wikipedia.org/wiki/Expected_value) given by the nodes and edges immediately preceding it. This mode is deterministic, and it will produce the same outcome each time it is run. 

###### Discrete
Intuitively, this can be thought as moving around a finite number of tokens, such as game pieces, from one space to another, where the destination of each move is based solely on chance.

When updating, the new weight of each node is the number of tokens on it. Each token is indivisible, and where it moves is determined by the outcome of a random event. This mode produces different results each run. The more tokens a simulation has, the more closely it approximates the Continuous mode.

###### Game
This is not a true Markov chain, since the transition process is not entirely random. In this mode, there are always two tokens. The goal of the player who controls the red token is to catch the blue token in as few iterations as possible.

A player can affect the token's transition probabilities by clicking on a node that is immediately downstream of the token's current position. Doing so will increase the likelihood by going to that node by 10 percentage points. This can only be done a limited amount of times, and the player accumulates one use per turn. Transition probabilities reset after each iteration. When the blue token is caught, the game is over.

### Dependencies
_Ignore this section if you are running the .exe file rather than the .py file._

This package requires the third-party package "pyyaml" to read the configuration files, which use the YAML markup language and have the file extension ".yaml".

### History and Plans
This grew out of a self-directed honors programs project at my community college a few years ago. While the original version did meet my needs, it was very poorly structured. For example, the so-called "configuration" files made function calls to the main file. Furthermore, some poor design choices prevented me from adding certain additional features (e.g. layouts) without major modifications to the core of the program.

This is the result of rewriting the project from the ground up. As of the full release, all of the functionality of the original has been replicated, and then some. Still, I plan to add additional features and improve the program in other ways with future releases. See backlog.md for more details.