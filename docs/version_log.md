### Full Releases

###### 1.5.0: Game Configs and Timer
- In Marko Polo, iterations now occur at regular time intervals.
- Game ends once Evader is caught.
- Configuration files can contain a new item: Game Settings (has 4 subitems).
    - Red Start: starting location of the pursuing player's token.
    - Blue Start: starting location of the target token.
    - Timer: time in milliseconds between iteration.
    - Points per Iteration: the number of points gained per iteration.
- Player's points are displayed in the bottom-left.

- Refactored Simulation.process_config to return a dictionary rather than modify one.

###### 1.4.0: Game Mode *
- Introduced a game mode (a.k.a. "Marko Polo").
    - Player controls a red token.
    - Goal is to catch the blue token in as few iterations as possible.
    - Left-click on a node to "tilt" toward it--i.e. increase your chance of going to it.
        - Cannot tilt toward a node that there was originally no chance of going to.
        - Number of tilts is limited by a player's points, which are spent with each tilt but accumulate each iteration.
    - Node weights are not visible, but edge weights are.
    
    
- Sped up drawing (via Screen.tracer).
- Refactored the code for transitions.
    - Created a Token class, representing a single object that is subject to the Markov system.
- Extensively refactored the code for UI updates.
    - Streamlined the interface.
    - Gave each node and edge their own Turtles.

###### 1.3.1: Discrete Mode Bugfixes *
- For Discrete mode, using "skip until stable" button always jumps to the iteration limit. 
- Continuous mode works properly for n > 1 tokens. (Already worked for n = 1.)


- Command line interface rewritten to use argparse module. 

###### 1.3.0: Monte Carlo Mode
- Added a discrete/Monte Carlo mode. The node weights are whole numbers, which are independently, probabilistically moved each time step. 

- Refactored MakovChain.py to reduce duplicated code.

###### 1.2.0: Display Toggling, Standalone Support *
- Press "w" to hide or show the node and edge weights.
- Added a standalone executable, "MarkovSim_v1-2-0.exe".
    - An updated EXE is indicated in future versions by an asterisk.


- Added scripts to easily create builds of the project.
- Project directory structure has been refactored.

###### 1.1.2: Sample File Improvements
- Modified configs to increase variety.
- Added some recommendations about sample files to the README.
- Reformatted the version log to increase clarity.

###### 1.1.1: Documentation Improvements
- Made several minor clarity improvements to the README file.

###### 1.1.0: Sample Files and Default Options Update
- Added interesting example configuration files:
    - _absorbing_chain_2:_ a complicated absorbing chain
    - _forest:_ a collection of three unconnected chains, including an oscillator
    - _minimal_config:_ an absorbing chain specified by an extremely simple config file
    - _monopoly:_ a Monopoly board with simplified movement rules
- Renamed "sink.yaml" to "absorbing_chain_1.yaml" to better reflect mathematical terminology.
- Made Layout Radius a configurable option.
- Added a default Dot Size based on number of nodes.
- Made Single the default Distribution option.
    - In such cases, the Distribution Details item is set to 0.
- Options item is now entirely optional.
- Refactored configs to make better use of defaults and other option varieties.
- Removed duplicate labeling of symmetrical edges.

###### 1.0.0: Full Release
*Note: configuration files made for beta versions are not compatible with full release versions.*

- Return key now instantly iterates the simulation until it's stable.
- Space key rebound to perform individual iterations.
- Window includes a scrollbar in cases where screen does not fit entire chain.
- Window is pseudo-maximized upon start-up.
- Added default values for Layout and Distribution options.
- Added defaults for Names and Distribution items.
- Added support for using node names within the Transitions item.
- Improved locations of edge labels.
- Added complex_weather.yaml.


- Revised "Uniform Distribution" config option to "Distribution", and gave it the possible values of "Uniform", "Single", and "Custom"
- Renamed weather.yaml to simple_weather.yaml
- Added some tolerance to the distribution and transition sum checks.
- Revised sample configuration files to eliminate unnecessary items and comply with new naming conventions.
- Completed and revised docstrings and file headers.
- Changed version number conventions. Beta versions are now 0.x.y, and full versions are 1.x.y.
- Added a helper.py, which defines functions which perform common tasks, such as finding coordinates on a circle.
- Added a buglist file.

### Beta

###### 0.4.1
- Updated and revised README.
- Adjusted rounded_demo to make it more interesting.

###### 0.4.0
- Added Reuleaux/rounded polygon layout (i.e. "Rounded").
- Added rounded_demo sample file.
- Increased LAYOUT_RADIUS to reduce crowding.

###### 0.3.2
- Fixed weather.yaml to comply with new config standards.

###### 0.3.1
- Added additional validation checks for config files
- Renamed Locations object to "Layout Details"
- Improved Configurations section of README.

###### 0.3.0
- Nodes display their names
- Numbers have been moved and reformatted (removed leading 0) to look good with names
- Changed node colors to improve visual clarity when overlayed by numbers
- Nodes have visible outlines
- Added constants file
- Added "market_types" config file, adjusted "sink" and "weather" files
- Iteration box moved to be relative to top-left corner of window

###### 0.2.0
- Added support for sparse transitions format
- Added a Polygon layout option value
    - Number of sides is specified in unique object called "Layout Details"
- Converted print + sys.exit calls to raise ValueErrors

###### 0.1.1
- Added sum validation checks for Transitions and Distribution items
- Adjusted sink.yaml to better represent the variety of options

###### 0.1.0
- Added support for Uniform Distribution option
- Added support for Circle layout
- Added "sink" sample configuration

###### 0.0.5
- Cleaned up MarkovChain init method, created "create_model" method
- Fixed and standardized the documentation heading style
- Cleaned up README

###### 0.0.4
- Added a section to README explaining the configuration items
- Added class descriptions in src files
- Removed vestigial "update_nodes" method from Simulation

###### 0.0.3
- Removed edge offsets in cases of single edges or symmetric edges
- Symmetric edges are drawn on top of one-another
- Added backlog

###### 0.0.2

- Removed delay from turtle
- Iteration number now displayed upon opening of window
- Removed the debugging printout of the configuration object
- Added changelog

###### 0.0.1

- Corrected usage statement
- Expanded README
- Restructured project directories
- Added venv/ to .gitignore

###### 0.0.0

Initial version.