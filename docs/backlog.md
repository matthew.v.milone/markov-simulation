### Modes

- Marko Polo
    - Add multiplayer support.
        - Iterations should occur regularly, not on user command.
        - Enable online support
    - Display each player's current points.
    - Allow for games that are multiple rounds long.
    
- Editor mode

### Configuration Features

- Move Distribution Details and similar items inside of Options.
- Support alternative layouts
    - Auto-fit (concentric rings with highest-degree nodes in center)
    - Combination (supports combinations of structures, such as two side-by-side polygons)
- Allow variables
- Allow arithmetic
- Add support for names within Distribution item
- Hyperchains
- Add a "Uniform Transition Probabilities" option

### Display Features

- Add way to adjust spacing of nodes
- Improve visual clarity in cases where node names overlap with edges 
- Properly maximize window on start-up

### User Interaction

- Improve versatility of command line interface--particularly mode argument.
- Add options for printing out discrete (or continuous?) paths.
- Improve iterate_until_stable timeout to account for absorbing chains when run in Discrete mode.

### Refactoring

- Replace custom code with calls to math.isclose, where applicable.
- Peruse the math module to find functions that decrease round-off error.

### Documentation

- Test and document support for alternative file types. Modify README accordingly.
- Add a license.
- Create and update missing or outdated docstrings.
- Add descriptions of the constants in constants.py.
- Fix the .gitignore and remove unnecessary files from remote repository. 
- Add member descriptions. (?)